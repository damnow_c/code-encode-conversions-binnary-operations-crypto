﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class SHL10
    {
        public static List<bool> SHL10Crypting(List<bool> boolMessage)
        {
            List<bool> boolMessageAfterSHL = new List<bool>();

            int k = 0;
            foreach (var singleBool in boolMessage)
            {
                k++;
                if (k > 10)
                {
                    boolMessageAfterSHL.Add(singleBool);
                }
            }
            for (int j = 0; j < 10; j++)
            {
                boolMessageAfterSHL.Add(boolMessage[j]);
            }

            return boolMessageAfterSHL;
        }

        public static List<bool> SHL10Decrypting(List<bool> boolMessage)
        {
            List<bool> boolMessageBoforeSHL = new List<bool>();
            int i = 0;
            int k = 0;

            do
            {
                boolMessageBoforeSHL.Add(boolMessage[boolMessage.Count() - 10 + i]);
                i++;
            } while (i < 10);

            foreach (var singleBool in boolMessage)
            {
                k++;
                if (k <= boolMessage.Count() - 10)
                {
                    boolMessageBoforeSHL.Add(singleBool);
                }
            }

            return boolMessageBoforeSHL;
        }
    }
}
