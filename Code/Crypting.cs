﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class Crypting
    {
        public static MessageWithKey CryptingWithoutPrintingSteps (List<char> message)
        {
            MessageWithKey cryptedMessage = 
                Cesar.CesarRightTwo(
                    HEX.IntToHex(
                        Binary.ChangeBoolListToIntList(
                            SHL10.SHL10Crypting(
                                Binary.OperationNOT(
                                    Binary.ChangeShortIntListToBoolsList(
                                        XOR2999.XOR(
                                            ASCII.LettersToASCII(message))))))));
            return cryptedMessage;
        }

        public static MessageWithKey CryptingWithPrintingSteps(List<char> message)
        {
            MessageWithKey cryptedMessage =
                Print.PrintMessageWithKey("After cesar",Cesar.CesarRightTwo(
                     Print.PrintMessageWithKey("After int->Hex", HEX.IntToHex(
                        Print.PrintIntList("After Binary->Int", Binary.ChangeBoolListToIntList(
                            Print.PrintBoolList("After SHL(10)", SHL10.SHL10Crypting(
                                Print.PrintBoolList("After NOT", Binary.OperationNOT(
                                    Print.PrintBoolList("After Int->Binary", Binary.ChangeShortIntListToBoolsList(
                                        Print.PrintIntList("After XOR(2999)", XOR2999.XOR(
                                            Print.PrintIntList("After Char->ASCII", ASCII.LettersToASCII(message))))))))))))))));
            return cryptedMessage;
        }
    }
}
