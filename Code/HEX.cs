﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class HEX
    {
        public static MessageWithKey IntToHex (List<int> letterValue)
        {
            MessageWithKey message = new MessageWithKey() { LetterValues = new List<int>(), KeyValues = new List<int>() };
            List<int> letterValueAfterModulo = new List<int>();
            List<int> restOfModulo = new List<int>();

            foreach (var singleLetterV in letterValue)
            {
                letterValueAfterModulo.Add(singleLetterV % (90 - 65) + 65);
                restOfModulo.Add(singleLetterV / (90 - 65));
            }

            foreach (var item in letterValueAfterModulo)
            {
                message.LetterValues.Add(item);
            }

            foreach (var item in restOfModulo)
            {
                message.KeyValues.Add(item);
            }

            return message;
        }

        public static List<int> HexToInt (MessageWithKey message)
        {
            List<int> letterValueAfterReverseModulo = new List<int>();
            int i = 0;

            foreach (var singleMessageLetterValue in message.LetterValues)
            {
                letterValueAfterReverseModulo.Add(25 * message.KeyValues[i] + singleMessageLetterValue - 65);
                i++;
            }

            return letterValueAfterReverseModulo;
        }
    }
}
