﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class Print
    {
        public static List<int> PrintIntList (string title, List<int> list)
        {
            Console.WriteLine($"\n\n{title}:\n");
            foreach (var number in list)
            {
                Console.Write(number + " ");
            }
            return list;
        }

        public static List<bool> PrintBoolList(string title, List<bool> list)
        {
            Console.WriteLine($"\n\n{title}:\n");
            foreach (var letter in list)
            {
                Console.Write(letter + " ");
            }
            return list;
        }

        public static MessageWithKey PrintMessageWithKey(string title, MessageWithKey message)
        {
            Console.WriteLine($"\n\n{title}:\n");
            Console.Write("\nMessage: ");
            foreach (var letter in message.LetterValues)
            {
                Console.Write(Convert.ToChar(letter) + " ");
            }
            Console.Write("\nKey: ");
            foreach (var key in message.KeyValues)
            {
                Console.Write(key + " ");
            }
            return message;
        }

        public static void PrintCharList(string title, List<char> list)
        {
            Console.WriteLine($"\n\n{title}:\n");
            foreach (var letter in list)
            {
                Console.Write(letter + " ");
            }
        }
    }
}
