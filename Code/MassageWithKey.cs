﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    public class MessageWithKey
    {
        public List<int> LetterValues { get; set; }
        public List<int> KeyValues { get; set; } //From modulo operation
    }
}
