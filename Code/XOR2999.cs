﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    public class XOR2999
    {
        public static List<int> XOR(List<int> intMessage)
        {
            List<int> intMessageAfterXOR = new List<int>();
            int i = 0;
            int localNumberFirst = 0;
            int localNumberSecond = 0;
            int localNumberThird = 0;
            bool overHundred = false;

            foreach (var number in intMessage)
            {
                localNumberFirst = 0;
                localNumberSecond = 0;
                localNumberThird = 0;
                overHundred = false;

                localNumberFirst = number % 10;
                localNumberSecond = (number % 100 - localNumberFirst) / 10;

                if (number > 100)
                {
                    localNumberThird = (number - localNumberFirst - localNumberSecond) / 100;
                    overHundred = true;
                }

                if (overHundred)
                {
                    if (i % 4 == 0)
                    {
                        intMessageAfterXOR.Add(localNumberThird ^ 2);
                    }
                    else
                    {
                        intMessageAfterXOR.Add(localNumberThird ^ 9);
                    }

                    i++;
                    if (i % 4 == 0)
                    {
                        intMessageAfterXOR.Add(localNumberSecond ^ 2);
                    }
                    else
                    {
                        intMessageAfterXOR.Add(localNumberSecond ^ 9);
                    }

                    i++;
                    if (i % 4 == 0)
                    {
                        intMessageAfterXOR.Add(localNumberFirst ^ 2);
                    }
                    else
                    {
                        intMessageAfterXOR.Add(localNumberFirst ^ 9);
                    }

                    i++;
                }
                else
                {
                    if (i % 4 == 0)
                    {
                        intMessageAfterXOR.Add(localNumberSecond ^ 2);
                    }
                    else
                    {
                        intMessageAfterXOR.Add(localNumberSecond ^ 9);
                    }

                    i++;
                    if (i % 4 == 0)
                    {
                        intMessageAfterXOR.Add(localNumberFirst ^ 2);
                    }
                    else
                    {
                        intMessageAfterXOR.Add(localNumberFirst ^ 9);
                    }

                    i++;
                }
            }

            return intMessageAfterXOR;
        }

        public static List<int> XORDecrypting(List<bool> boolMessage)
        {
            List<int> intLetterBeforeXOR = new List<int>();
            List<int> hexMessageAfterXor = new List<int>();
            List<int> intMessageBeforeXOR = new List<int>();
            int i = 0;
            int numberHex = 0;

            int j = 0;
            foreach (var item in boolMessage)
            {
                if (item)
                {
                    numberHex += Convert.ToInt32(Math.Pow(2, Convert.ToDouble(3-j)));
                }
                j++;
                if (j == 4)
                {
                    hexMessageAfterXor.Add(numberHex);
                    numberHex = 0;
                    j = 0;
                }
            }
            

            foreach (var number in hexMessageAfterXor)
            {
                if (i % 4 == 0)
                {
                    intLetterBeforeXOR.Add(number ^ 2);
                }
                else
                {
                    intLetterBeforeXOR.Add(number ^ 9);
                }

                i++;
            }

            for (int k = 0; k < intLetterBeforeXOR.Count/2; k++)
            {
                intMessageBeforeXOR.Add(10 * intLetterBeforeXOR[2 * k] + intLetterBeforeXOR[2 * k + 1]);
            }

            return intMessageBeforeXOR;
        }
    }
}
