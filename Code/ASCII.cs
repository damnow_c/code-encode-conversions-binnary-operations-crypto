﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class ASCII
    {
        public static List<int> LettersToASCII (List<char> liftOfChars)
        {
            List<int> intMessage = new List<int>();

            foreach (var letter in liftOfChars)
            {
                intMessage.Add(Convert.ToInt32(letter));
            }

            return intMessage;
        }

        public static List<char> ASCIIToChars (List<int> lettersInAscii)
        {
            List<char> charMessage = new List<char>();

            foreach (var letter in lettersInAscii)
            {
                charMessage.Add(Convert.ToChar(letter));
            }

            return charMessage;
        }
    }
}
