﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class Binary
    {
        public static List<bool> ChangeShortIntListToBoolsList (List<int> intMessage)
        {
            List<bool> boolMessage = new List<bool>();

            foreach (var number in intMessage)
            {
                var array = Convert.ToString(number, 2).Select(s => s.Equals('1')).ToArray();

                int whileCase = array.Length;

                while (whileCase < 4)
                {
                    boolMessage.Add(false);
                    whileCase++;
                }

                foreach (var a in array)
                {
                    boolMessage.Add(a);
                }
            }

            return boolMessage;
        }

        public static List<bool> ChangeIntListToBoolsList(List<int> intMessage)
        {
            List<bool> boolMessage = new List<bool>();

            foreach (var number in intMessage)
            {
                var array = Convert.ToString(number, 2).Select(s => s.Equals('1')).ToArray();

                int whileCase = array.Length;

                while (whileCase < 8)
                {
                    boolMessage.Add(false);
                    whileCase++;
                }

                foreach (var a in array)
                {
                    boolMessage.Add(a);
                }
            }

            return boolMessage;
        }

        public static List<bool> OperationNOT(List<bool> boolMessage)
        {
            List<bool> boolMessageAfterNot = new List<bool>();

            foreach (var singleBool in boolMessage)
            {
                boolMessageAfterNot.Add(!singleBool);
            }
            
            return boolMessageAfterNot;
        }

        public static List<int> ChangeBoolListToIntList (List<bool> boolMessage)
        {

            List<bool> singleByte = new List<bool>();
            List<int> letterValue = new List<int>();
            int singleLetterValue = 0;
            int l = 0;

            foreach (var singleBool in boolMessage)
            {

                if (singleBool)
                {
                    singleLetterValue += Convert.ToInt32(Math.Pow(Convert.ToDouble(2), Convert.ToDouble(7 - l)));
                }

                l++;

                if (l == 8)
                {
                    l = 0;
                    letterValue.Add(singleLetterValue);
                    singleLetterValue = 0;
                }
            }

            return letterValue;
        }

    }
}
