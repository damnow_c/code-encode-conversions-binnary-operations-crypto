﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Code
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Choose option:\n1. Code and decode massage and see every single conversion. \n2. Code message.\n3. Decode message.");
                int key = 0;
                do
                {
                    key = 0;
                    key = Convert.ToInt32(Console.ReadLine());
                } while (key != 1 && key != 2 && key != 3);
                
                Console.Clear();
                Console.Write("Enter message: ");

                if ( key == 1 || key == 2)
                {
                    List<char> charMessage = Console.ReadLine().ToUpper().ToCharArray().ToList();
                    if (key == 1)
                    {
                        MessageWithKey codedMessage = Crypting.CryptingWithPrintingSteps(charMessage);
                        Print.PrintMessageWithKey("Encoded message", codedMessage);
                        List<char> decodedMessage = Decrypting.DecryptingWithPriningSteps(codedMessage);
                        Print.PrintCharList("Decoded message", decodedMessage);
                    }
                    else
                    {
                        MessageWithKey codedMessage = Crypting.CryptingWithoutPrintingSteps(charMessage);
                        Print.PrintMessageWithKey("Encoded message", codedMessage);
                    }
                }
                else
                {
                    MessageWithKey message = new MessageWithKey(){LetterValues = new List<int>(), KeyValues = new List<int>()};
                    message.LetterValues = ASCII.LettersToASCII(Console.ReadLine().ToUpper().ToCharArray().ToList());
                    Console.Write("Enter keys values: ");
                    List<char> charMessage = Console.ReadLine().ToUpper().ToCharArray().ToList();
                    foreach (var singleChar in charMessage)
                    {
                        message.KeyValues.Add(Convert.ToInt32(singleChar)-48);
                    }
                    List<char> decodedMessage = Decrypting.DecryptingWithoutPriningSteps(message);
                    Print.PrintCharList("Decoded message", decodedMessage);
                }
                Console.WriteLine("\n\nPress any key to continue.");
                Console.ReadLine();
            } while (true);


        }
    }
}
