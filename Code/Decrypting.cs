﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    class Decrypting
    {
        public static List<char> DecryptingWithoutPriningSteps(MessageWithKey message)
        {
            List<char> decryptedMessage =
                ASCII.ASCIIToChars(
                    XOR2999.XORDecrypting(
                            Binary.OperationNOT(
                                SHL10.SHL10Decrypting(
                                    Binary.ChangeIntListToBoolsList(
                                        HEX.HexToInt(
                                            Cesar.CesarLeftTwo(message)))))));
            return decryptedMessage;
        }

        public static List<char> DecryptingWithPriningSteps(MessageWithKey message)
        {
            List<char> decryptedMessage =
                ASCII.ASCIIToChars(
                    Print.PrintIntList("After XOR(2999)", XOR2999.XORDecrypting(
                            Print.PrintBoolList("After NOT", Binary.OperationNOT(
                                Print.PrintBoolList("After SHL(10)", SHL10.SHL10Decrypting(
                                    Print.PrintBoolList("After Int->Binary", Binary.ChangeIntListToBoolsList(
                                        Print.PrintIntList("After Hex->Int", HEX.HexToInt(
                                            Print.PrintMessageWithKey("After cesar", Cesar.CesarLeftTwo(message)))))))))))));
            return decryptedMessage;
        }
    }
}
