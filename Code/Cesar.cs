﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Code
{
    public class Cesar
    {
        public static MessageWithKey CesarRightTwo (MessageWithKey message)
        {
            List<int> letterValueAfterCesar = new List<int>();
            int valueInForech = 0;

            foreach (var singleLetterV in message.LetterValues)
            {
                valueInForech = singleLetterV;
                if (singleLetterV == 89)
                {
                    valueInForech = 63;
                }
                if (singleLetterV == 90)
                {
                    valueInForech = 64;
                }

                letterValueAfterCesar.Add(valueInForech + 2);
                valueInForech = 0;
            }

            for (int i = 0; i < message.LetterValues.Count(); i++)
            {
                message.LetterValues[i] = letterValueAfterCesar[i];
            }

            return message;
        }

        public static MessageWithKey CesarLeftTwo(MessageWithKey message)
        {
            List<int> letterValueAfterCesar = new List<int>();
            int valueInForech = 0;

            foreach (var singleLetterV in message.LetterValues)
            {
                valueInForech = singleLetterV;
                if (singleLetterV == 65)
                {
                    valueInForech = 91;
                }
                if (singleLetterV == 66)
                {
                    valueInForech = 92;
                }

                letterValueAfterCesar.Add(valueInForech - 2);
                valueInForech = 0;
            }

            for (int i = 0; i < message.LetterValues.Count(); i++)
            {
                message.LetterValues[i] = letterValueAfterCesar[i];
            }

            return message;
        }
    }
}
